package simpleFrame;

public class Kwadrat extends Figura {

    public double getA() {
        return a;
    }


    private double a;

    public Kwadrat(double a) {
        this.a = a;
    }

    @Override //@Override przypomina o zaimplementowaniu metod z figury
    public double podajZ() {
        return a;
    }

    @Override
    public double podajX() {
        return a;
    }

    public double obliczPole() {
        return 4 * a;
    }
}
