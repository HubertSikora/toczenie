package simpleFrame;

public abstract class Figura {
    public abstract double obliczPole();
    public abstract double podajZ();
    public abstract double podajX();
}
