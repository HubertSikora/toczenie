package simpleFrame;

public class Prostokat extends Figura {
    //gettery i stettery
    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    //zmienne
    private double z;
    private double x;

    //konstuktor
    public Prostokat(double osZ, double osX) {
        this.z = osZ;
        this.x = osX;
    }


    @Override
    public double obliczPole() {
        return z*x;
    }

    @Override
    public double podajZ() {
        return z;

    }

    public double podajX() {
        return x;
    }
}
