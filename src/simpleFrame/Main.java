package simpleFrame;

import java.util.Scanner;

/**
 * Created by Hubert on 2017-09-18.
 */
public class Main {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj ilośc figur jakie mozesz wyszczegolic w czesci");
        int iloscFigur = sc.nextInt();
        int ktoraFigura = 0;

        Figura[] tablicaFigur = new Figura[iloscFigur];


        boolean czyWyjsc = false;


        while (!czyWyjsc) {

            System.out.println("Wybierz opcję :");
            System.out.println("1 - Kwadrat;");
            System.out.println("2 - Prostokat;");
            System.out.println("90 - Test wyswietl tablice;");
            System.out.println("91 - Test: oblicz pole oddzielnie dla kazdej");
            System.out.println("92 - Test: oblicz pole calkowite");
            System.out.println("93 - Test: oblicz sume Z");
            System.out.println("0 - Zapisz i przejdź dalej ");


            int choice = sc.nextInt();
            Figura figura;
            switch (choice) {
                case 1: {
                    System.out.println("Podaj długość boku");
                    double bok = sc.nextDouble();
                    figura = new Kwadrat(bok);
                    tablicaFigur[ktoraFigura++] = figura;
                    break;
                }
                case 2: {
                    System.out.println("Podaj długość boku Z następnie X");
                    double bokZ = sc.nextDouble();
                    double bokX = sc.nextDouble();
                    figura = new Prostokat(bokZ, bokX);
                    tablicaFigur[ktoraFigura++] = figura;
                    break;
                }
                case 90: {

                    for (int i = 0; i < tablicaFigur.length; i++)
                        System.out.println(tablicaFigur[i]);
                    break;
                }
                case 91: {
                    for (int i = 0; i < iloscFigur; i++) {
                        Figura f = tablicaFigur[i];
                        System.out.println("Pole figury nr" + i + ": " + f.obliczPole());
                    }
                    break;
                }
                case 92: {
                    double sumaPol = 0;
                    for (int i = 0; i < tablicaFigur.length; i++) {
                        Figura f = tablicaFigur[i];
                        sumaPol += f.obliczPole();
                    }
                    System.out.println("Pole całości to: " + sumaPol);
                    break;
                }
                case 93: {
                    double sumaZ = 0;
                    for (int i = 0; i < tablicaFigur.length; i++) {
                        Figura f = tablicaFigur[i];
                        sumaZ += f.podajZ();
                    }
                    System.out.println("Suma po osi Z: " + sumaZ);

                    break;
                }

                case 0:
                    czyWyjsc = true;
                    break;
                default:
                    System.out.println("Cos poszlo nie tak!");

            }
        }         // podaj dlugosc(suma) do ktoregos tam elementu tablicy
        System.out.println("Toczenie wstępne");
        System.out.println("Podaj do którego elementu przeprowadzić pierwszą operację");
//            int doKtorego = sc.nextInt();
//            for (int i = 0; i < doKtorego; i++) {
//                sumOfTablica += tablicaFigur[i];
//            }
//            System.out.println("Dlugosc po osi Z: " + sumOfTablica);
    }
}
